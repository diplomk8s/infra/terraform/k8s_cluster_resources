terraform {
  required_providers {
    vkcs = {
      source  = "vk-cs/vkcs"      
    }
  }
}

provider "vkcs" {
  username   = var.username_for_cloud_provider
  password   = var.password_for_cloud_provider
  project_id = var.project_id_for_cloud_provider
  region     = var.region_for_cloud_provider
}
