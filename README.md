Для создания инфраструктуры в облаке Mail необходимо:

1) склонировать себе репозиторий

git clone git@gitlab.com:diplomk8s/infra/terraform/k8s_cluster_resources.git

или 

git clone https://gitlab.com/diplomk8s/infra/terraform/k8s_cluster_resources.git

2) зарегистрировать раннер(например, shell runner) по инструкции https://docs.gitlab.com/runner/install/

3) в домашней директории раннера(/home/gitlab-runner) необходимо создать файл .terraformrc со следующим содержимым


provider_installation {
  network_mirror {
    url = "https://hub.mcs.mail.ru/repository/terraform-providers/"
    include = ["vk-cs/*"]
  }
  direct {
    exclude = ["vk-cs/*"]
  }
}


4) добавить credetials в переменные проекта
5)  запустить pipeline



Кол-во ресурсов,серверов итп можно изменять с помощью файла variables.yml
