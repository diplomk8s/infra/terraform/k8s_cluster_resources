resource "vkcs_networking_network" "compute" {
  name = var.compute_name_network
}

resource "vkcs_networking_subnet" "compute" {
  name       = var.compute_name_subnet
  network_id = vkcs_networking_network.compute.id
  cidr       = var.compute_cidr_subnet
  ip_version = var.compute_ip_version
}

resource "vkcs_networking_router" "compute" {
  name                = var.compute_name_vm_router
  admin_state_up      = var.compute_admin_state_up
  external_network_id = data.vkcs_networking_network.extnet.id
}

resource "vkcs_networking_router_interface" "compute" {
  router_id = vkcs_networking_router.compute.id
  subnet_id = vkcs_networking_subnet.compute.id
}

resource "vkcs_compute_keypair" "user" {
  name       = var.username
  public_key = file(var.public_key)
}

resource "vkcs_networking_secgroup" "my-security_group" {
  name = var.name_security_group
}

resource "vkcs_networking_secgroup_rule" "secgroup_rule_1" {
  for_each          = toset(var.ingress_allow_ports)
  port_range_min    = each.key
  port_range_max    = each.key
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = var.protocol_security_group_ingress
  remote_ip_prefix  = var.remote_ip_prefix_security_group_ingress
  security_group_id = vkcs_networking_secgroup.my-security_group.id
}
resource "vkcs_networking_secgroup_rule" "secgroup_rule_2" {
  port_range_min    = 30000
  port_range_max    = 32767
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = var.protocol_security_group_ingress
  remote_ip_prefix  = var.remote_ip_prefix_security_group_ingress
  security_group_id = vkcs_networking_secgroup.my-security_group.id
}

resource "vkcs_compute_instance" "master" {
  count             = var.count_masters
  name              = "master-${count.index}"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  security_groups   = [var.name_security_group]
  availability_zone = var.availability_zone_master
  key_pair          = var.username

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = var.source_type_master_disk1
    destination_type      = var.destination_type_master_disk1
    volume_type           = var.volume_type_master_disk1
    volume_size           = var.volume_size_master_disk1
    boot_index            = var.boot_index_master_disk1
    delete_on_termination = var.delete_on_termination_master_disk1
  }

  network {
    uuid = vkcs_networking_network.compute.id
  }

  depends_on = [
    vkcs_networking_network.compute,
    vkcs_networking_subnet.compute
  ]
}

resource "vkcs_compute_instance" "worker" {
  count             = var.count_workers
  name              = "worker-${count.index}"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  security_groups   = [var.name_security_group]
  availability_zone = var.availability_zone_worker
  key_pair          = var.username

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = var.source_type_worker_disk1
    destination_type      = var.destination_type_worker_disk1
    volume_type           = var.volume_type_worker_disk1
    volume_size           = var.volume_size_worker_disk1
    boot_index            = var.boot_index_worker_disk1
    delete_on_termination = var.delete_on_termination_worker_disk1
  }

  network {
    uuid = vkcs_networking_network.compute.id
  }

  depends_on = [
    vkcs_networking_network.compute,
    vkcs_networking_subnet.compute
  ]
}

resource "vkcs_compute_instance" "ingress" {
  count             = var.count_ingress
  name              = "ingress-${count.index}"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  security_groups   = [var.name_security_group]
  availability_zone = var.availability_zone_ingress
  key_pair          = var.username

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = var.source_type_ingress_disk1
    destination_type      = var.destination_type_ingress_disk1
    volume_type           = var.volume_type_ingress_disk1
    volume_size           = var.volume_size_ingress_disk1
    boot_index            = var.boot_index_ingress_disk1
    delete_on_termination = var.delete_on_termination_ingress_disk1
  }

  network {
    uuid = vkcs_networking_network.compute.id
  }

  depends_on = [
    vkcs_networking_network.compute,
    vkcs_networking_subnet.compute
  ]
}

resource "vkcs_compute_instance" "lb" {
  count             = var.count_lb
  name              = "lb-${count.index}"
  flavor_id         = data.vkcs_compute_flavor.compute.id
  security_groups   = [var.name_security_group]
  availability_zone = var.availability_zone_lb
  key_pair          = var.username

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = var.source_type_lb_disk1
    destination_type      = var.destination_type_lb_disk1
    volume_type           = var.volume_type_lb_disk1
    volume_size           = var.volume_size_lb_disk1
    boot_index            = var.boot_index_lb_disk1
    delete_on_termination = var.delete_on_termination_lb_disk1
  }

  network {
    uuid = vkcs_networking_network.compute.id
  }

  depends_on = [
    vkcs_networking_network.compute,
    vkcs_networking_subnet.compute
  ]
}

resource "vkcs_networking_floatingip" "fip" {
  count = var.count_masters
  pool  = data.vkcs_networking_network.extnet.name
}

resource "vkcs_networking_floatingip" "fip_ingress" {
  count = var.count_ingress
  pool  = data.vkcs_networking_network.extnet.name
}

resource "vkcs_networking_floatingip" "fip_worker" {
  count = var.count_workers
  pool  = data.vkcs_networking_network.extnet.name
}

resource "vkcs_networking_floatingip" "fip_lb" {
  count = var.count_lb
  pool  = data.vkcs_networking_network.extnet.name
}

resource "vkcs_compute_floatingip_associate" "fip" {
  count       = var.count_masters
  floating_ip = vkcs_networking_floatingip.fip[count.index].address
  instance_id = vkcs_compute_instance.master[count.index].id
}

resource "vkcs_compute_floatingip_associate" "fip_worker" {
  count       = var.count_workers
  floating_ip = vkcs_networking_floatingip.fip_worker[count.index].address
  instance_id = vkcs_compute_instance.worker[count.index].id
}

resource "vkcs_compute_floatingip_associate" "fip_ingress" {
  count       = var.count_ingress
  floating_ip = vkcs_networking_floatingip.fip_ingress[count.index].address
  instance_id = vkcs_compute_instance.ingress[count.index].id
}

resource "vkcs_compute_floatingip_associate" "fip_lb" {
  count       = var.count_lb
  floating_ip = vkcs_networking_floatingip.fip_lb[count.index].address
  instance_id = vkcs_compute_instance.lb[count.index].id
}
