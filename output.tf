output "instance_fips_master" {
  value = vkcs_networking_floatingip.fip.*.address
}

output "instance_fips_worker" {
  value = vkcs_networking_floatingip.fip_worker.*.address
}

output "instance_fips_ingress" {
  value = vkcs_networking_floatingip.fip_ingress.*.address
}

output "instance_fips_lb" {
  value = vkcs_networking_floatingip.fip_lb.*.address
}
