variable "username_for_cloud_provider" {
  description = "username"
}

variable "password_for_cloud_provider" {
  description = "password"
}

variable "project_id_for_cloud_provider" {
  description = "project_id"
}

variable "region_for_cloud_provider" {
  default = "RegionOne"
}

variable "username" {
  default = "iceforest"
}

variable "public_key" {
  default = "public_key/id_rsa.pub"
}
variable "cpu_ram_disk" {
  default = "Standard-2-4-40"
}
variable "image" {
  default = "Ubuntu-18.04-Standard"
}
variable "networkvm" {
  default = "ext-net"
}

variable "compute_admin_state_up" {
  type    = bool
  default = true
}

variable "compute_ip_version" {
  default = 4
}

variable "ingress_allow_ports" {
  description = "list of ports to open server"
  type        = list(any)
  default     = ["80", "22", "81", "3000", "9090", "9093", "9094", "9100", "24224", "3100", "9080", "6443", "2379", "2380", "8080", "10250", "10259", "10257", "9153", "9555", "8443", "7000", "7070", "5050", "5000", "50051", "3550", "6379", "179", "9099", "6666", "6667", "53", "67", "68"]
}

variable "count_masters" {
  default = 3
}

variable "count_workers" {
  default = 2
}

variable "count_ingress" {
  default = 1
}

variable "count_lb" {
  default = 1
}

variable "name_security_group" {
  default = "my-security_group"
}

variable "compute_cidr_subnet" {
  default = "192.168.199.0/24"
}

variable "compute_name_subnet" {
  default = "subnet_1"
}

variable "compute_name_network" {
  default = "compute-net"
}

variable "compute_name_vm_router" {
  default = "vm_router"
}

variable "availability_zone_master" {
  default = "GZ1"
}

variable "availability_zone_worker" {
  default = "GZ1"
}

variable "availability_zone_ingress" {
  default = "GZ1"
}

variable "availability_zone_lb" {
  default = "GZ1"
}

variable "protocol_security_group_ingress" {
  default = "tcp"
}

variable "remote_ip_prefix_security_group_ingress" {
  default = "0.0.0.0/0"
}

variable "source_type_master_disk1" {
  default = "image"
}

variable "destination_type_master_disk1" {
  default = "volume"
}

variable "volume_type_master_disk1" {
  default = "ceph-ssd"
}

variable "volume_size_master_disk1" {
  default = 8
}

variable "boot_index_master_disk1" {
  default = 0
}

variable "delete_on_termination_master_disk1" {
  type    = bool
  default = true
}

variable "source_type_worker_disk1" {
  default = "image"
}

variable "destination_type_worker_disk1" {
  default = "volume"
}

variable "volume_type_worker_disk1" {
  default = "ceph-ssd"
}

variable "volume_size_worker_disk1" {
  default = 8
}

variable "boot_index_worker_disk1" {
  default = 0
}

variable "delete_on_termination_worker_disk1" {
  type    = bool
  default = true
}

variable "source_type_ingress_disk1" {
  default = "image"
}

variable "destination_type_ingress_disk1" {
  default = "volume"
}

variable "volume_type_ingress_disk1" {
  default = "ceph-ssd"
}

variable "volume_size_ingress_disk1" {
  default = 8
}

variable "boot_index_ingress_disk1" {
  default = 0
}

variable "delete_on_termination_ingress_disk1" {
  type    = bool
  default = true
}

variable "source_type_lb_disk1" {
  default = "image"
}

variable "destination_type_lb_disk1" {
  default = "volume"
}

variable "volume_type_lb_disk1" {
  default = "ceph-ssd"
}

variable "volume_size_lb_disk1" {
  default = 8
}

variable "boot_index_lb_disk1" {
  default = 0
}

variable "delete_on_termination_lb_disk1" {
  type    = bool
  default = true
}
