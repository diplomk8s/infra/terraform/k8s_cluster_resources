data "vkcs_networking_network" "extnet" {
  name = var.networkvm
}

data "vkcs_compute_flavor" "compute" {
  name = var.cpu_ram_disk
}

data "vkcs_images_image" "compute" {
  name = var.image
}
